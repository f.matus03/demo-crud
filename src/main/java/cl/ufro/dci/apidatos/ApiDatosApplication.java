package cl.ufro.dci.apidatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDatosApplication.class, args);
	}

}
