package cl.ufro.dci.apidatos.repository;

import cl.ufro.dci.apidatos.model.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

}
