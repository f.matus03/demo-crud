package cl.ufro.dci.apidatos.controller;

import cl.ufro.dci.apidatos.model.Persona;
import cl.ufro.dci.apidatos.repository.PersonaRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@CrossOrigin(origins = "http: ")
@RestController

public class PersonaController {
    private final PersonaRepository personaRepository;

    public PersonaController(PersonaRepository personaRepository){
        this.personaRepository = personaRepository;
    }
    @PostMapping(value = "/personas")
        public boolean guardarPersona(@RequestBody Persona persona){
        System.out.println(persona.getPerNombre());
        personaRepository.save(persona);
        return true;
    }

    @GetMapping(value = "/personas")
        public Iterable<Persona> obtenerPersonas(){
        return personaRepository.findAll();
    }

    @DeleteMapping(value = "/personas/remover/{personaId}")
    public boolean removerPersona(@PathVariable Long personaId) {
        Optional<Persona> personaOptional = personaRepository.findById(personaId);

        if (personaOptional.isPresent()) {
            Persona persona = personaOptional.get();
            personaRepository.delete(persona);
            return true;
        }
        return false;
    }

    @PutMapping(value = "/personas/editar/{personaId}")
    public boolean EditarPersona(@RequestBody JsonNode json, @PathVariable Long personaId){
        Optional<Persona> personaOptional = personaRepository.findById(personaId);
        if (personaOptional.isPresent()) {
            Persona persona = personaOptional.get();
            persona.setPerCorreo(json.get("perCorreo").asText());
            persona.setPerNombre(json.get("perNombre").asText());
            personaRepository.save(persona);
            return true;
        }
        return false;
    }

    @GetMapping(value = "/personas/recibir/{personaId}")
    public Persona recibirPersona(@PathVariable Long personaId){
        Optional<Persona> personaOptional = personaRepository.findById(personaId);
        if (personaOptional.isPresent()) {
            Persona persona = personaOptional.get();
            return persona;
        }
        return null;
    }
}
