package cl.ufro.dci.apidatos.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long perId;
    private String perNombre;
    private String perCorreo;
}
